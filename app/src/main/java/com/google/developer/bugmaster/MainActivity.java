package com.google.developer.bugmaster;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.developer.bugmaster.data.DatabaseManagerLoader;
import com.google.developer.bugmaster.data.Insect;
import com.google.developer.bugmaster.data.InsectRecyclerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final String KEY_STATE_SORTING = "sortingAlphabetically";

    private InsectRecyclerAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ArrayList<Insect> mAssortedInsects = new ArrayList<>();
    private boolean mIsAlphabeticallySorted = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new InsectRecyclerAdapter(null);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_STATE_SORTING)) {
            mIsAlphabeticallySorted = savedInstanceState.getBoolean(KEY_STATE_SORTING);
        }

        showInsectList(mIsAlphabeticallySorted);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_STATE_SORTING, mIsAlphabeticallySorted);
    }

    private void showInsectList(boolean sortAlphabetically) {
        Bundle args = new Bundle();
        args.putString("sortBy", !sortAlphabetically ? "dangerLevel" : "name");
        getSupportLoaderManager().initLoader(0, args, this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                Bundle args = new Bundle();
                mIsAlphabeticallySorted = !mIsAlphabeticallySorted;
                args.putString("sortBy", !mIsAlphabeticallySorted ? "dangerLevel" : "name");
                getSupportLoaderManager().restartLoader(0, args, this);
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* Click events in Floating Action Button */
    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, QuizActivity.class);
        Collections.shuffle(mAssortedInsects);
        ArrayList<Insect> options = new ArrayList<>(mAssortedInsects.subList(0, QuizActivity.ANSWER_COUNT));
        i.putExtra(QuizActivity.EXTRA_INSECTS, options);
        i.putExtra(QuizActivity.EXTRA_ANSWER, options.get(ThreadLocalRandom.current().nextInt(0, QuizActivity.ANSWER_COUNT)));
        startActivity(i);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, final Bundle args) {
        return new DatabaseManagerLoader(MainActivity.this, args.getString("sortBy"));
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.moveToFirst()) {
            do {
                Insect i = new Insect(data);
                mAssortedInsects.add(i);
            } while (data.moveToNext());
        }
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAssortedInsects.clear();
        mAdapter.swapCursor(null);
    }
}
