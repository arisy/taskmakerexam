package com.google.developer.bugmaster.data;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Created by adani on 01/05/2018.
 */

public class DatabaseManagerLoader extends AsyncTaskLoader<Cursor> {

    private final String mSortBy;
    private Cursor mCursor;

    public DatabaseManagerLoader(Context context, String sortBy) {
        super(context);
        mSortBy = sortBy;
    }

    @Override
    public Cursor loadInBackground() {
        mCursor = DatabaseManager.getInstance(getContext()).queryAllInsects(mSortBy);
        return mCursor;
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged() || mCursor == null) {
            forceLoad();
        }
    }
}
