package com.google.developer.bugmaster.data;

import android.content.Context;
import android.database.Cursor;

/**
 * Singleton that controls access to the SQLiteDatabase instance
 * for this application.
 */
public class DatabaseManager {
    private static DatabaseManager sInstance;

    public static synchronized DatabaseManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseManager(context.getApplicationContext());
        }

        return sInstance;
    }

    private BugsDbHelper mBugsDbHelper;

    private DatabaseManager(Context context) {
        mBugsDbHelper = new BugsDbHelper(context);
    }

    /**
     * Return a {@link Cursor} that contains every insect in the database.
     *
     * @param sortOrder Optional sort order string for the query, can be null
     * @return {@link Cursor} containing all insect results.
     */
    public Cursor queryAllInsects(String sortOrder) {
        String sortMethod = "ASC";
        if (sortOrder.equals("dangerLevel")) {
            sortMethod = "DESC";
        }
        return mBugsDbHelper.getReadableDatabase().query(Insect.TABLE_NAME,
                new String[]{"name", "scientificName", "classification", "imageAsset", "dangerLevel"},
                null, null, null, null, sortOrder + " " + sortMethod);
    }

    /**
     * Return a {@link Cursor} that contains a single insect for the given unique id.
     *
     * @param id Unique identifier for the insect record.
     * @return {@link Cursor} containing the insect result.
     */
    public Cursor queryInsectsById(int id) {
        return mBugsDbHelper.getReadableDatabase().query(Insect.TABLE_NAME,
                new String[]{"name", "scientificName", "classification", "imageAsset", "dangerLevel"},
                "_id=" + id, null, null, null, null);
    }
}
