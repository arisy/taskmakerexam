package com.google.developer.bugmaster;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.developer.bugmaster.data.Insect;
import com.google.developer.bugmaster.databinding.ActivityInsectDetailsBinding;

import java.io.IOException;
import java.io.InputStream;

public class InsectDetailsActivity extends AppCompatActivity {

    public static final String KEY_EXTRA_INSECT = "insect";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityInsectDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_insect_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Insect insect = getIntent().getParcelableExtra(KEY_EXTRA_INSECT);
        binding.setInsect(insect);

        try (InputStream imageInputStream = getAssets().open(insect.imageAsset)) {
            Bitmap bitmap = BitmapFactory.decodeStream(imageInputStream);
            binding.ivInsect.setImageBitmap(bitmap);
        } catch (IOException e) {
            Log.e(InsectDetailsActivity.class.getSimpleName(), e.getMessage(), e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
