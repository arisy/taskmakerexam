package com.google.developer.bugmaster.data;

import android.content.Intent;
import android.database.Cursor;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.developer.bugmaster.InsectDetailsActivity;
import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.views.DangerLevelView;

/**
 * RecyclerView adapter extended with project-specific required methods.
 */

public class InsectRecyclerAdapter extends
        RecyclerView.Adapter<InsectRecyclerAdapter.InsectHolder> {

    public void swapCursor(Cursor data) {
        if (mCursor != null) {
            mCursor.close();
        }
        mCursor = data;
        notifyDataSetChanged();
    }

    /* ViewHolder for each insect item */
    public class InsectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView commonNameTv;
        TextView scienceNameTv;
        DangerLevelView dangerLevelView;

        public InsectHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            commonNameTv = (TextView) itemView.findViewById(R.id.common_name_view);
            scienceNameTv = (TextView) itemView.findViewById(R.id.scientific_name_tv);
            dangerLevelView = (DangerLevelView) itemView.findViewById(R.id.danger_level);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), InsectDetailsActivity.class);
            intent.putExtra(InsectDetailsActivity.KEY_EXTRA_INSECT, (Parcelable) itemView.getTag());
            v.getContext().startActivity(intent);
        }
    }

    private Cursor mCursor;

    public InsectRecyclerAdapter(Cursor c) {
        mCursor = c;
    }

    @Override
    public InsectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_insect, parent, false);
        return new InsectHolder(view);
    }

    @Override
    public void onBindViewHolder(InsectHolder holder, int position) {
        Insect item = getItem(position);
        holder.commonNameTv.setText(item.name);
        holder.dangerLevelView.setDangerLevel(item.dangerLevel);
        holder.scienceNameTv.setText(item.scientificName);
        holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return mCursor == null? 0 : mCursor.getCount();
    }

    /**
     * Return the {@link Insect} represented by this item in the adapter.
     *
     * @param position Adapter item position.
     * @return A new {@link Insect} filled with this position's attributes
     * @throws IllegalArgumentException if position is out of the adapter's bounds.
     */
    public Insect getItem(int position) {
        if (position < 0 || position >= getItemCount()) {
            throw new IllegalArgumentException("Item position is out of adapter's range");
        } else if (mCursor.moveToPosition(position)) {
            return new Insect(mCursor);
        }
        return null;
    }
}
