package com.google.developer.bugmaster.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import com.google.developer.bugmaster.R;

public class DangerLevelView extends TextView {

    private int mDangerLevel;

    public DangerLevelView(Context context) {
        super(context);
        initDefault();
    }

    public DangerLevelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initDefault();
    }

    public DangerLevelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initDefault();
    }

    public DangerLevelView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initDefault();
    }

    public void initDefault() {
        setTextAppearance(android.R.style.TextAppearance_Large);
        setGravity(Gravity.CENTER);
        setTextColor(getResources().getColor(android.R.color.white));
        setBackgroundResource(R.drawable.background_danger);
    }

    public void setDangerLevel(int dangerLevel) {
        mDangerLevel = dangerLevel;
        setText(String.valueOf(mDangerLevel));
        String[] dangerColors = getResources().getStringArray(R.array.dangerColors);

        if (dangerLevel > 0 && dangerLevel <= dangerColors.length) {
            Drawable compatBackground = DrawableCompat.wrap(getBackground());
            DrawableCompat.setTint(compatBackground, Color.parseColor(dangerColors[dangerLevel - 1]));
        }
    }

    public int getDangerLevel() {
        return mDangerLevel;
    }
}
