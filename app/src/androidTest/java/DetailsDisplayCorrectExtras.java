import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.developer.bugmaster.InsectDetailsActivity;
import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.data.Insect;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by adani on 01/05/2018.
 */
@RunWith(AndroidJUnit4.class)
public class DetailsDisplayCorrectExtras {

    @Rule
    public ActivityTestRule<InsectDetailsActivity> rule = new ActivityTestRule<InsectDetailsActivity>(InsectDetailsActivity.class, false, false);

    @Test
    public void testInsectExtrasProperlyDisplayed1() {
        Insect i1 = new Insect("Black Widow", "Latrodectus mactans", "Arachnida", "spider.png", 10);
        Intent incomingIntent = new Intent();
        incomingIntent.putExtra(InsectDetailsActivity.KEY_EXTRA_INSECT, i1);
        rule.launchActivity(incomingIntent);

        onView(withId(R.id.tv_common_name)).check(matches(withText((i1.name))));
        onView(withId(R.id.tv_classification)).check(matches(withText((rule.getActivity().getString(R.string.classification_label_format, i1.classification)))));
        onView(withId(R.id.tv_scientific_name)).check(matches(withText((i1.scientificName))));
    }

    @Test
    public void testInsectExtrasProperlyDisplayed2() {
        Insect i2 = new Insect("Brown Recluse", "Loxosceles reclusa", "Arachnida", "spider.png",10);
        Intent incomingIntent = new Intent();
        incomingIntent.putExtra(InsectDetailsActivity.KEY_EXTRA_INSECT, i2);
        rule.launchActivity(incomingIntent);

        onView(withId(R.id.tv_common_name)).check(matches(withText((i2.name))));
        onView(withId(R.id.tv_classification)).check(matches(withText((rule.getActivity().getString(R.string.classification_label_format, i2.classification)))));
        onView(withId(R.id.tv_scientific_name)).check(matches(withText((i2.scientificName))));
    }
}
