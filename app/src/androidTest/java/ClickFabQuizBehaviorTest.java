import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.developer.bugmaster.MainActivity;
import com.google.developer.bugmaster.QuizActivity;
import com.google.developer.bugmaster.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by adani on 01/05/2018.
 */

@RunWith(AndroidJUnit4.class)
public class ClickFabQuizBehaviorTest {

    @Rule
    public IntentsTestRule<MainActivity> rule = new IntentsTestRule<>(MainActivity.class);

    @Test
    public void testFabGoToQuiz() {
        onView(withId(R.id.fab)).perform(click());

        intended(IntentMatchers.hasComponent(hasClassName(QuizActivity.class.getName())));
    }
}
